import React from 'react';
import { StyleSheet, Text, View } from 'react-native';


import * as firebase from 'firebase';
const firebaseConfig = {
  apiKey: "AIzaSyCIdgXlRv6kYeWSjnEJtIYePprDD1DQuvM",
  authDomain: "react-firebase-2253c.firebaseapp.com",
  databaseURL: "https://react-firebase-2253c.firebaseio.com",
  projectId: "react-firebase-2253c",
  storageBucket: ""
};
firebase.initializeApp(firebaseConfig);
import  { container,Content,Header,Form,Input,Item,Button,Label, Container } from 'native-base';





 class App extends React.Component {
   constructor(props){
     super(props)
     this.state = ({
       email : '',
       password : ''

     });
   }



   signUpUser=(email,password)=>{
try {

  if(this.state.password.length < 6){
    alert("Please enter at least 6 chareacter")
    return;
  }
  firebase.auth().createUserWithEmailAndPassword(email,password);
  
} catch (error) {
  alert(error.toString());
}
   }

   
   logInUser=(email,password)=>{
try {

  firebase.auth().signInWithEmailAndPassword(email,password).then(function(user){
  console.log(user);
  })
  
} catch (error) {
  console.log(error.toString())
}
  }


  render() {
    return (
   <Container style={styles.container}>
    <Form>
      <Item floatingLabel>
        <Label>Email</Label>
        <Input
        autoCorrect = {false}
        autoCapitalize = "none"
        onChangeText = {(email)=>this.setState({email})}
        />

        </Item>
        <Item floatingLabel>
        <Label>Password</Label>
        <Input
        password={true}
        secureTextEntry = {true}
        autoCorrect = {false}
        autoCapitalize = "none"
        onChangeText = {(password) => this.setState({password})}
        />

        </Item>


       <Button style ={styles.btnStyle}
        block success full
        onPress = {()=>this.logInUser(this.state.email,this.state.password)}
        >
            <Text style = {{color:'white'}}>Login</Text>
          </Button>





       <Button style ={styles.btnStyle}
        block info full
        onPress = {() => this.signUpUser(this.state.email,this.state.password)}
        >
            <Text style = {{color:'white'}}>Signup</Text>
          </Button>

      </Form>

     </Container>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#fff',
    justifyContent: 'center',
  },
  btnStyle :{
    marginTop : 10,
    padding:10,
    marginLeft:10,
    marginRight:10,
    borderRadius: 64,
  }
});

export default App;
